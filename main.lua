-- chikun :: 2014
-- Ludum Dare 30


function love.load()

    -- Load up required libraries
    require "lib/loadLibs"

    -- Load game objects
    require "src/init"

    -- Load core functions
    core.load()

    -- Start state manager
    state.load()

    print(tonumber(bit.bxor(1, 1)))

end


function love.update(dt)


    -- Induce lag if below 15 FPS
    if dt > 1 / 15 then
        dt = 1 / 15
    end

    -- Update core functions
    core.update(dt)

    -- Update state
    state.update(dt)

end


function love.draw()

    -- Draw underlay
    core.underlay()

    -- Draw functions
    state.draw()

    -- Draw overlay
    core.overlay()

end
