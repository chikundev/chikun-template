-- chikun :: 2014
-- Menu state


-- Temporary state, removed at end of script
local menuState = { }


-- On state create
function menuState:create()

    menuVars = {
        selected  = 1,
        timer     = 0,
        openTimer = 0.2,

        menuCode = {
            function()  -- Start new game
                menuVars.startGame = true
            end,
            function() -- Options menu
                menuVars.options  = true
            end,
            function() -- Exit game
                e.quit()
            end
        },

        menuColors = { -- Colors for the menu:
            sel   = {255, 255, 255}, -- Selected text; and
            unsel = {128, 128, 128}  -- Unselected text
        }
    }

end


-- On state update
function menuState:update(dt)

    -- Timer on menu open to make sure things can't be selected before we want them to
    if menuVars.openTimer > 0 then
        menuVars.openTimer = menuVars.openTimer - dt
    elseif menuVars.openTimer < 0 then
        menuVars.openTimer = 0
    end

    -- Timer to make sure the menu doesn't scroll wantonly
    if menuVars.timer >= 1 then
        if menuVars.timer >= 1.1 then
            menuVars.timer = 0
        else
            menuVars.timer = menuVars.timer + dt
        end
    end

    -- Scroll through the menu
    if input.check("down") and menuVars.timer == 0 then
        menuVars.selected = menuVars.selected + 1
        menuVars.timer = 1
        sfx.menuMove:stop()
        sfx.menuMove:play()
    elseif input.check("up") and menuVars.timer == 0 then
        menuVars.selected = menuVars.selected - 1
        menuVars.timer = 1
        sfx.menuMove:stop()
        sfx.menuMove:play()
    -- Select highlighted menu option
    elseif input.check("action") and menuVars.openTimer <= 0 then
        menuVars.menuCode[menuVars.selected]()
        sfx.menuBlip:stop()
        sfx.menuBlip:play()
    end

    -- Loop scrolling bottom to top and vice-versa
    if menuVars.selected > 3 then
        menuVars.selected = 1
    elseif menuVars.selected < 1 then
        menuVars.selected = 3
    end

    -- Also start the game (shhh)
    if menuVars.startGame then
        state.change(states.intro)
    -- Also open the options (shhh)
    elseif menuVars.options then
        state.change(states.options)
    end

end


-- On state draw
function menuState:draw()

        -- Update scaling
    scale.perform()

    -- Draw bg
    g.setColor(255, 255, 255)


    -- Draw menu text
    if menuVars.selected == 1 then
        g.setColor(menuVars.menuColors.sel)
    else
        g.setColor(menuVars.menuColors.unsel)
    end
    if s.getOS() == "Android" then
        g.printf("Start", 0, 100, 550, "right")
    else
        g.printf("Start", 0, 220, 550, "right")
    end

    if menuVars.selected == 2 then
        g.setColor(menuVars.menuColors.sel)
    else
        g.setColor(menuVars.menuColors.unsel)
    end
    if s.getOS() == "Android" then
        g.printf("Options" , 0, 130, 550, "right")
    else
        g.printf("Options", 0, 250, 550, "right")
    end

    if menuVars.selected == 3 then
        g.setColor(menuVars.menuColors.sel)
    else
        g.setColor(menuVars.menuColors.unsel)
    end
    if s.getOS() == "Android" then
        g.printf("Exit Game", 0, 160, 550, "right")
    else
        g.printf("Exit Game", 0, 280, 550, "right")
    end

-- g.print(menuVars.selected .. " " .. menuVars.timer)

end


-- On state kill
function menuState:kill()

    -- Kill menuVars
    menuVars = nil

    bgm.mainMenu:pause()

end


-- Transfer data to state loading script
return menuState
