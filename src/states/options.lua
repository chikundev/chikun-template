-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local menuState = { }


-- On state create
function menuState:create()

    menuVars = {
        selected = 1,
        timer    = 0,
        openTimer = 0.2,

        menuCode = {
            function() -- To the intro!
                menuVars.intro = true
            end,

            function() -- Blank function for the scale options
            end,

            function() -- Blank function for the volume options
            end,

            function() -- Back to main menu
                menuVars.mainMenu = true
            end
        },

        menuColors = { -- Colors for the menu:
            sel   = {255, 255, 255}, -- Selected text; and
            unsel = {128, 128, 128}  -- Unselected text
        },

        scaleModes    = {"whole", "full", "ratio"},
        scaleSelected = nil,

        -- currVol = volume

    }

    -- Find current selected based on scaleModes
    for key, mode in ipairs(menuVars.scaleModes) do
        if mode == scale.mode then
            menuVars.scaleSelected = key
        end
    end


end


-- On state update
function menuState:update(dt)

    if menuVars.openTimer > 0 then
        menuVars.openTimer = menuVars.openTimer - dt
    elseif menuVars.openTimer < 0 then
        menuVars.openTimer = 0
    end

    -- Timer to make sure the menu doesn't scroll wantonly
    if menuVars.timer >= 1 then
        if menuVars.timer >= 1.1 then
            menuVars.timer = 0
        else
            menuVars.timer = menuVars.timer + dt
        end
    end

    -- Scroll through the menu
    if input.check("down") and menuVars.timer == 0 then
        menuVars.selected = menuVars.selected + 1
        menuVars.timer = 1
        sfx.menuMove:stop()
        sfx.menuMove:play()
    elseif input.check("up") and menuVars.timer == 0 then
        menuVars.selected = menuVars.selected - 1
        menuVars.timer = 1
        sfx.menuMove:stop()
        sfx.menuMove:play()
    -- Scroll left and right to change scale option
    elseif input.check("right") and menuVars.timer == 0 then
        if menuVars.selected == 2 then
            menuVars.scaleSelected = menuVars.scaleSelected + 1
        elseif menuVars.selected == 3 then
            volume = volume + 0.1
            if volume > 1 then
                volume = 1
            else
                sfx.pop:play()
            end
        end
        menuVars.timer = 1
    elseif input.check("left") and menuVars.timer == 0 then
        if menuVars.selected == 2 then
            menuVars.scaleSelected = menuVars.scaleSelected - 1
            menuVars.timer = 1
        elseif menuVars.selected == 3 then
            volume = volume - 0.1
            if volume < 0.1 then
                volume = 0
            else
                sfx.pop:stop()
                sfx.pop:play()
            end
        end
        menuVars.timer = 1

    -- Select highlighted menu option
    elseif input.check("action") and menuVars.openTimer <= 0 then
        menuVars.menuCode[menuVars.selected]()
        sfx.menuBlip:stop()
        sfx.menuBlip:play()
    end

    -- Loop scrolling bottom to top and vice-versa
    if menuVars.selected > 4 then
        menuVars.selected = 1
    elseif menuVars.selected < 1 then
        menuVars.selected = 4
    end

    -- Loop scrolling left to right and vice-versa
    if menuVars.scaleSelected > 3 then
        menuVars.scaleSelected = 1
    elseif menuVars.scaleSelected < 1 then
        menuVars.scaleSelected = 3
    end

    -- Code to keep scale.mode up to date
    scale.mode = menuVars.scaleModes[menuVars.scaleSelected]

    -- Keep the volume here and in core.lua synced
    menuVars.currvol = volume

    -- Code to return to main menu
    if menuVars.mainMenu then
        state.change(states.menu)

    -- Code to head on to the intro
    elseif menuVars.intro then
        state.change(states.credits)
    end


end


-- On state draw
function menuState:draw()

    -- Update scaling
    scale.perform()

    -- Draw bg
    g.setColor(255, 255, 255)
    g.draw(gfx.planets, 0, 0)
    g.draw(gfx.introTitle, 0, 0)

    -- Draw menu text
    g.setFont(fnt.menu)
    if menuVars.selected == 1 then
        g.setColor(menuVars.menuColors.sel)
    else
        g.setColor(menuVars.menuColors.unsel)
    end
    if s.getOS() == "Android" then
        g.printf("Credits", 0, 70, 550, "right")
    else
        g.printf("Credits", 0, 190, 550, "right")
    end

    if menuVars.selected == 2 then
        g.setColor(menuVars.menuColors.sel)
    else
        g.setColor(menuVars.menuColors.unsel)
    end
    if s.getOS() == "Android" then
        g.printf("Scale Mode: " .. scale.mode, 0, 100, 550, "right")
    else
        g.printf("Scale Mode: " .. scale.mode, 0, 220, 550, "right")
    end

    if menuVars.selected == 3 then
        g.setColor(menuVars.menuColors.sel)
    else
        g.setColor(menuVars.menuColors.unsel)
    end
    if s.getOS() == "Android" then
        g.printf("Volume: " .. volume * 100 .. "%", 0, 130, 550, "right")
    else
        g.printf("Volume: " .. volume * 100 .. "%", 0, 250, 550, "right")
    end

    if menuVars.selected == 4 then
        g.setColor(menuVars.menuColors.sel)
    else
        g.setColor(menuVars.menuColors.unsel)
    end
    if s.getOS() == "Android" then
        g.printf("Main Menu", 0, 160, 550, "right")
    else
        g.printf("Main Menu", 0, 280, 550, "right")
    end

end


-- On state kill
function menuState:kill()

    menuVars = nil

end


-- Transfer data to state loading script
return menuState
