-- chikun :: 2014
-- Splash state


-- Temporary state, removed at end of script
local splashState = { }


-- On state create
function splashState:create()

    --
    splashVars = {
        timer   = 1,
        played  = false
    }

end


-- On state update
function splashState:update(dt)

    -- Decrease timer
    splashVars.timer = splashVars.timer - dt

    -- If timer is empty, change state to menu
    if splashVars.timer < 0 then
        state.change(states.menu)
    elseif splashVars.timer <= 0.5 and not splashVars.played then
        sfx.startup:play()
        splashVars.played = true
    end

end


-- On state draw
function splashState:draw()
    -- Calculate useable alpha value
    local alpha = 1 - math.abs(splashVars.timer - 0.5) * 2

    -- Draw chikun logo in middle of screen
    g.setFont(fnt.splash)
    g.setColor(255, 255, 255, alpha * 255)
    g.printf("chikun", 0, 296, 1280, "center")

end


-- On state kill
function splashState:kill()

    -- Kill splashVars
    splashVars = nil

end


-- Transfer data to state loading script
return splashState
