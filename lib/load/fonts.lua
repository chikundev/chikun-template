-- chikun :: 2014
-- Loads all fonts from the /fnt folder


-- Font multiplier
local multi = 1

-- Can't do this recursively due to sizes
fnt = {
    -- Splash screen font
    splash  = love.graphics.newFont("fnt/exo2.otf", 128 * multi),
}
